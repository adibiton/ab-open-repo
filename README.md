## ab-open-repo
A simple cli to open a git repository url

- tested with gitlab, github, bitbucket
- works on multi-platform (osx, windows)

#### how to install
``` bash
    npm i -g ab-open-repo
```

#### how to use
- run the following command from directory with .git file

``` bash
    open-repo
```
