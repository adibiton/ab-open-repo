#!/usr/bin/env node
'use strict'
const debug = require('debug')('ab-open-repo')
const opn = require('opn')
const execa = require('execa')
const extractRepoUrl = require('./lib/extract-repo-url')
const openProject = () => {
  try {
    const remoteUrl = execa.sync('git', ['remote', 'get-url', 'origin']).stdout
    const repoUrl = extractRepoUrl(remoteUrl)
    opn(repoUrl)
  } catch (e) {
    if (debug.enabled) {
      throw new Error('Error while reading git configuration: ', e)
    } else {
      console.log('The directory is not a valid git repository')
    }
  } finally {
    process.exit(1)
  }
}

module.exports = openProject()
