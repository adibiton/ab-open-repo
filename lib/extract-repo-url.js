'use strict'
const {ERROR_NOT_VALID_GIT_CONFIGURATION} = require('./constants')

module.exports = (remoteURl) => {
  let url, user, repo
  if (remoteURl.startsWith('git')) {
    [url, user, repo] = remoteURl.match(/^git@(.+\.(?:com|org)):(.+)\/(.+).git/i).slice(1)
  } else if (remoteURl.startsWith('http')) {
    [url, user, repo] = remoteURl.match(/^http(?:s):\/\/(.+)\/(.+)\/(.+).git/i).slice(1)
  } else {
    throw new Error(ERROR_NOT_VALID_GIT_CONFIGURATION)
  }
  return `https://www.${url}/${user}/${repo}`
}
