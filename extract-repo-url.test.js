const extractRepoUrl = require('./lib/extract-repo-url')
const {ERROR_NOT_VALID_GIT_CONFIGURATION} = require('./lib/constants')

describe('github', () => {
  test('ssh', () => {
    expect(extractRepoUrl('git@github.com:adibiton/string-calc.git'))
      .toBe('https://www.github.com/adibiton/string-calc')
  })
  test('http', () => {
    expect(extractRepoUrl('https://github.com/adibiton/string-calc.git'))
      .toBe('https://www.github.com/adibiton/string-calc')
  })
  test('Try to extract not valid url', () => {
    expect(() => extractRepoUrl('tig@github.com:adibiton/string-calc.git'))
      .toThrow(new Error(ERROR_NOT_VALID_GIT_CONFIGURATION))
  })
})
describe('gitlab', () => {
  test('ssh', () => {
    expect(extractRepoUrl('git@gitlab.com:adibiton/ab-open-repo.git'))
      .toBe('https://www.gitlab.com/adibiton/ab-open-repo')
  })
  test('http', () => {
    expect(extractRepoUrl('https://gitlab.com/adibiton/ab-open-repo.git'))
      .toBe('https://www.gitlab.com/adibiton/ab-open-repo')
  })
})
describe('bitbucket', () => {
  test('ssh', () => {
    expect(extractRepoUrl('git@bitbucket.org:adi-biton/test-bb.git'))
      .toBe('https://www.bitbucket.org/adi-biton/test-bb')
  })
  test('http', () => {
    expect(extractRepoUrl('https://adi-biton@bitbucket.org/adi-biton/test-bb.git'))
      .toBe('https://www.adi-biton@bitbucket.org/adi-biton/test-bb')
  })
})
